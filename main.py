def print_name(user_name: str) -> str:
    return user_name

def calculator1(first_num: int,second_num: int,third_num: int) -> int:
    sum = first_num+second_num+third_num
    return sum

def return_string() -> str:
    return "Hello World"

def calculator2(first_num: float,second_num: float) -> float:
    return first_num-second_num


print(print_name("Andrzej"))
print(calculator1(4,2,4))
print(return_string())
print(calculator2(3.5,4.7))

class Human:
    def __init__(self,first_name: str,last_name: str,age: int):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def print_personal_data(self):
         print(self.first_name," ",self.last_name," born ",self.age, " years ago.")

    def change_age(self,new_age: int):
        self.age = new_age
        print("Age value change was succesful")


person1=Human("Andrzej","Osowski","21")
person1.print_personal_data()

class Programmer(Human):
    def __init__(self, language: str, first_name: str, last_name: str, age: int):
        Human.__init__(self, first_name, last_name, age)
        self.language=language

person2=Programmer("Python","John","Kowalski",59)
person2.print_personal_data()
person2.change_age(45)
person2.print_personal_data()